package web

import (
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2"

	"github.com/danicheeta/gints/controllers"
	"github.com/gorilla/mux"
)

func NewServer(port string, router *mux.Router, db *mgo.Database) (err error) {

	authctl := controllers.NewAuth(router, db)
	authctl.Initialize()
	gamectl := controllers.NewGameAccessor(router, db)
	gamectl.Initialize()

	fmt.Println("listening to :" + port)
	err = http.ListenAndServe(":"+port, router)
	return
}

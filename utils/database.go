package utils

import "gopkg.in/mgo.v2"

type Config struct {
	Uri string
}

func NewDBSession(conf *Config) (*mgo.Session, error) {
	session, err := mgo.Dial(conf.Uri)
	return session, err
}

func GetDatabase(session *mgo.Session, name string) *mgo.Database {
	db := session.DB(name)
	defer session.Close()
	return db
}

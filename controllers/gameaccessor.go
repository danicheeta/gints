package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/danicheeta/gints/models"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type GameAccessorImpl struct {
	Router *mux.Router
	DB     *mgo.Database
}

func NewGameAccessor(router *mux.Router, db *mgo.Database) *GameAccessorImpl {
	return &GameAccessorImpl{Router: router, DB: db}
}

func (gai *GameAccessorImpl) Initialize() {
	gai.Router.HandleFunc("/getgames", gai.getGames)
	gai.Router.HandleFunc("/addgame", gai.addGame)
	gai.Router.HandleFunc("/addhint", gai.addHint)
	gai.Router.HandleFunc("/gethint", gai.getHints)
}

func (gai *GameAccessorImpl) getGames(w http.ResponseWriter, r *http.Request) {
	response := models.NewDefaultGame()
	GamesColl := gai.DB.C("games")
	err := GamesColl.Find(nil).All(&response)
	if err != nil {
		fmt.Println("no game collection was found")
	}

	data, _ := json.Marshal(response)
	w.Header().Set("content-type", "application/json")
	fmt.Fprint(w, string(data))
}

func (gai *GameAccessorImpl) addGame(w http.ResponseWriter, r *http.Request) {
	title := r.FormValue("title")
	imgurl := r.FormValue("imgurl")
	desc := r.FormValue("desc")
	game := models.NewGame(title, imgurl, desc)
	GamesColl := gai.DB.C("games")
	GamesColl.Insert(&game)
}

func (gai *GameAccessorImpl) addHint(w http.ResponseWriter, r *http.Request) {
	quer := bson.M{"title": r.FormValue("game")}
	desc := r.FormValue("desc")
	now := time.Now().String()
	change := bson.M{"$push": bson.M{"hints": bson.M{"description": desc, "date": now}}}
	err := gai.DB.C("games").Update(quer, change)

	if err != nil {
		fmt.Println("can not add a hint to this game")
	}
}

func (gai *GameAccessorImpl) getHints(w http.ResponseWriter, r *http.Request) {
	game := models.NewDefaultGame()
	gameName := r.FormValue("game")
	from, _ := strconv.Atoi(r.FormValue("from"))
	limit, _ := strconv.Atoi(r.FormValue("limit"))
	err := gai.DB.C("games").Find(bson.M{"title": gameName}).Select(bson.M{"hints": bson.M{"$slice": []int{from, limit}}}).One(&game)
	if err != nil {
		fmt.Println("cant get hints\n", err)
	}

	data, errjson := json.Marshal(game.Hints)
	if errjson != nil {
		fmt.Println("couldnt marshal hints\n", errjson)
	}

	w.Header().Set("content-type", "application/json")
	fmt.Fprint(w, string(data))
}

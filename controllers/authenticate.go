package controllers

import (
	"net/http"

	"github.com/danicheeta/gints/models"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
)

type UserImpl struct {
	Router *mux.Router
	DB     *mgo.Database
}

func NewAuth(router *mux.Router, db *mgo.Database) *UserImpl {
	return &UserImpl{Router: router, DB: db}
}

func (ui *UserImpl) Initialize() {
	ui.Router.HandleFunc("/login", ui.login).Methods("POST")
	ui.Router.HandleFunc("/register", ui.register).Methods("POST")
	ui.Router.HandleFunc("/auth", ui.authrize).Methods("POST")
}

func (ui *UserImpl) login(w http.ResponseWriter, r *http.Request) {

}

func (ui *UserImpl) register(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("user")
	password := r.FormValue("password")
	user := models.NewUser(username, password)
	user.Prepare()

	userStruct := struct{ users, passs string }{users: user.Username, passs: user.Passhash}
	ui.DB.C("users").Insert(&userStruct)
}

func (ui *UserImpl) authrize(w http.ResponseWriter, r *http.Request) {

}

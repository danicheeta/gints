package main

import (
	"os"

	"github.com/danicheeta/gints/utils"
	"github.com/danicheeta/gints/web"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
)

var db *mgo.Database

const MongoURI string = "mongodb://danicheeta:2331374@ds021650.mlab.com:21650/gips"

func main() {
	port := os.Getenv("PORT")
	router := mux.NewRouter()
	session, err := utils.NewDBSession(&utils.Config{Uri: MongoURI})
	if err != nil {
		print(err)
	}
	db = utils.GetDatabase(session, "gips")
	defer session.Close()

	web.NewServer(port, router, db)
}

package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Game struct {
	Id          bson.ObjectId `bson:"_id,omitempty"`
	Title       string        `json:"title"`
	Imgurl      string        `json:"imgurl"`
	Description string        `json:"desc"`
	Hints       []*hint       `bson:"hints", json:"hints"`
}

type hint struct {
	Description string    `json:"desc"`
	Date        time.Time `json:"date"`
}

func NewGame(title, imgurl, desc string) *Game {
	return &Game{Title: title, Imgurl: imgurl, Description: desc}
}

func NewDefaultGame() *Game {
	return &Game{}
}

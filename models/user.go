package models

import (
	"crypto/md5"
	"encoding/hex"
)

type User struct {
	Username string
	Password string
	Passhash string
}

func NewUser(user, pass string) *User {
	return &User{Username: user, Password: pass}
}

func (user *User) Prepare() {
	hash := md5.New()
	hash.Write([]byte(user.Username))
	user.Passhash = hex.EncodeToString(hash.Sum(nil))
}
